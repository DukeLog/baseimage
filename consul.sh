#!/bin/sh
_term() {
	kill -INT $pid
}
trap _term TERM

if [ -z $CONSUL_HOST ]; then
    echo "CONSUL_HOST not set, discovering from route"
    CONSUL_HOST=$(ip route show 0.0.0.0/0 | grep -Eo 'via \S+' | awk '{ print $2 }')
    if [ $CONSUL_HOST = "" ]; then
        echo "CONSUL_HOST discovery from ip route failed. Quitting.."
        exit 1
    fi
    echo $CONSUL_HOST > /etc/container_environment/CONSUL_HOST
fi

echo "Consul host is: $CONSUL_HOST"

DC=$(curl -sq http://$CONSUL_HOST:8500/v1/agent/self | jshon -e Config -e Datacenter 2> /dev/null |tr -d '"')
DOMAIN=$(curl -sq http://$CONSUL_HOST:8500/v1/agent/self | jshon -e Config -e Domain 2> /dev/null |tr -d '"')

if [ "$DC" = "" ] || [ "$DOMAIN" = "" ]; then 
    echo "Consul DC and domain discovery failed. Quitting.."
    exit 1
fi

echo $DC > /etc/container_environment/CONSUL_DC
echo $DOMAIN > /etc/container_environment/CONSUL_DOMAIN

cat /etc/container_environment/CONSUL_DC
/usr/local/bin/consul agent \
    -join $CONSUL_HOST \
    -datacenter $DC \
    -domain $DOMAIN \
    -config-file /etc/consul/consul.json \
    -config-dir /etc/consul/conf.d &
pid=$!
wait $pid